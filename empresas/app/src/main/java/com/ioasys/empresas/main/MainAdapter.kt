package com.ioasys.empresas.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ioasys.empresas.R
import com.ioasys.empresas.details.DetailsActivity
import com.ioasys.empresas.model.Enterprise
import com.ioasys.empresas.util.AppConstants
import com.ioasys.empresas.util.loadUrl
import kotlinx.android.synthetic.main.item_enterprise.view.*
import kotlinx.android.synthetic.main.item_enterprise.view.iv_photo

class MainAdapter(private var items: List<Enterprise>): RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private val context = itemView.context
        private lateinit var enterprise: Enterprise

        init {
            view.setOnClickListener (this)
        }

        private val name = view.tv_name
        private val type = view.tv_type
        private val country = view.tv_country
        private val photo = view.iv_photo

        fun bind(enterprise: Enterprise) {
            this.enterprise = enterprise

            name.text = enterprise.enterprise_name
            type.text = enterprise.enterprise_type.enterpriseTypeName
            country.text = enterprise.country

            enterprise.photo?.let {
                val url = "${AppConstants.baseUrl}$it"
                Glide.with(context)
                    .loadUrl(url)
                    .into(photo)
            }
        }

        override fun onClick(v: View?) {
            val bundleExtras = Bundle()
            bundleExtras.putParcelable(AppConstants.enterpriseKey, this.enterprise)

            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtras(bundleExtras)
            context.startActivity(intent)
        }
    }

    fun updateItems(items: List<Enterprise>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = ViewHolder (
        LayoutInflater.from(parent.context).inflate(R.layout.item_enterprise, parent, false)
    )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])
}