package com.ioasys.empresas.main

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.ioasys.empresas.R
import com.ioasys.empresas.model.Enterprise
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_toolbar.*
import kotlinx.android.synthetic.main.item_toolbar.view.*


class MainActivity : AppCompatActivity(), MainContract.View {

    private lateinit var presenter: MainContract.Presenter
    private lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addToolbar()
        setupUi()

        setPresenter(MainPresenter(this))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)

        menu?.let { setupSearchable(it) }

        return true
    }

    override fun setPresenter(presenter: MainContract.Presenter) {
        this.presenter = presenter
    }

    override fun getContext(): Context {
        return this
    }

    override fun displayEnterprises(enterprises: List<Enterprise>) {
        adapter.updateItems(enterprises)
    }

    private fun setupUi() {
        adapter = MainAdapter(listOf())
        rv_items.adapter = adapter
    }

    private fun addToolbar() {
        toolbar?.let {
            setSupportActionBar(it)
        }

        supportActionBar?.apply {
            setDisplayShowHomeEnabled(false)
            setDisplayShowTitleEnabled(false)
        }
    }

    private fun setupSearchable(menu: Menu) {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView.setOnSearchClickListener {
            toolbar.iv_logo.visibility = View.GONE

            supportActionBar?.apply {
                setDisplayShowHomeEnabled(false)
                setDisplayShowTitleEnabled(false)
                setDisplayHomeAsUpEnabled(false)
                setDisplayUseLogoEnabled(false)
            }
        }

        searchView.setOnCloseListener {
            toolbar.iv_logo.visibility = View.VISIBLE
            return@setOnCloseListener false
        }

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.clearFocus()
                presenter.search(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                return false
            }
        })
    }
}
