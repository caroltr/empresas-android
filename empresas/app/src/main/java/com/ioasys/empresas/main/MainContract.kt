package com.ioasys.empresas.main

import android.content.Context
import com.ioasys.empresas.base.BasePresenter
import com.ioasys.empresas.base.BaseView
import com.ioasys.empresas.model.Enterprise

interface MainContract {

    interface Presenter : BasePresenter {
        fun search(query: String)
    }

    interface View : BaseView<Presenter> {
        fun getContext(): Context
        fun displayEnterprises(enterprises: List<Enterprise>)
    }
}