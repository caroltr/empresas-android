package com.ioasys.empresas.main

import com.ioasys.empresas.model.Enterprises
import com.ioasys.empresas.network.ApiFactory
import com.ioasys.empresas.repository.Preferences
import com.ioasys.empresas.util.AppConstants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class MainPresenter(private val view: MainContract.View) : MainContract.Presenter {

    override fun search(query: String) {
        val headers = getHeaders()
        ApiFactory.api.enterpriseFilter(headers, query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                this::onSuccess,
                this::onError
            )
    }

    private fun onSuccess(response: Response<Enterprises>) {
        if (response.isSuccessful) {
            response.body()?.enterprises?.let {
                view.displayEnterprises(it)
            }

        } else {
            // TODO handle error
            onError(null)
        }
    }

    private fun onError(error: Throwable?) {
        // TODO display error
    }

    private fun getHeaders(): Map<String, String> {
        val headers = Preferences.getAuthHeaders(view.getContext())
        return mapOf(
            AppConstants.accessTokenKey to headers.accessToken,
            AppConstants.uidKey to headers.uid,
            AppConstants.clientKey to headers.client
        )
    }
}