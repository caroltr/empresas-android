package com.ioasys.empresas.model

import com.google.gson.annotations.SerializedName

data class Enterprises (
    @SerializedName("enterprises") val enterprises: List<Enterprise>
)