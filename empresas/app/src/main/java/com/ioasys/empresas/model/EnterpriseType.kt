package com.ioasys.empresas.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EnterpriseType (
    @SerializedName("id") val id: Int,
    @SerializedName("enterprise_type_name") val enterpriseTypeName: String
): Parcelable