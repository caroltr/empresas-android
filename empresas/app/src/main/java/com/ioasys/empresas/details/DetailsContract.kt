package com.ioasys.empresas.details

import android.os.Bundle
import com.ioasys.empresas.base.BasePresenter
import com.ioasys.empresas.base.BaseView
import com.ioasys.empresas.model.Enterprise

interface DetailsContract {

    interface Presenter : BasePresenter {
        fun start(bundle: Bundle?)
    }

    interface View : BaseView<Presenter> {
        fun displayDetails(enterprise: Enterprise)
    }
}