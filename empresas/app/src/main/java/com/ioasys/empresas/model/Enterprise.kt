package com.ioasys.empresas.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Enterprise (
    @SerializedName("id") val id: Long,
    @SerializedName("email_enterprise") val name: String?,
    @SerializedName("facebook") val facebook: String?,
    @SerializedName("twitter") val twitter: String?,
    @SerializedName("linkedin") val linkedin: String?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("own_enterprise") val own_enterprise: Boolean,
    @SerializedName("enterprise_name") val enterprise_name: String,
    @SerializedName("photo") val photo: String?,
    @SerializedName("description") val description: String,
    @SerializedName("city") val city: String,
    @SerializedName("country") val country: String,
    @SerializedName("value") val value: Double,
    @SerializedName("share_price") val share_price: Double,
    @SerializedName("enterprise_type") val enterprise_type: EnterpriseType
) : Parcelable