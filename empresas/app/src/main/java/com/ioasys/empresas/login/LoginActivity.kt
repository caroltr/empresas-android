package com.ioasys.empresas.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ioasys.empresas.R
import com.ioasys.empresas.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginContract.View {

    private lateinit var presenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupUi()
        setPresenter(LoginPresenter(this))

        btn_enter.setOnClickListener { btnEnterAction() }
    }

    override fun setPresenter(presenter: LoginContract.Presenter) {
        this.presenter = presenter
    }

    override fun navigateMainView() {
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(intent)
    }

    override fun getContext(): Context {
        return this
    }

    override fun startProgress() {
        btn_enter.isEnabled = false
        tv_error.visibility = View.GONE
    }

    override fun endProgress() {
        btn_enter.isEnabled = true
    }

    override fun displayError() {
        tv_error.visibility = View.VISIBLE
        tv_error.text = getString(R.string.login_error)
    }

    private fun setupUi() {
        tv_error.visibility = View.GONE
    }

    private fun btnEnterAction() {
        val email = et_email.text.toString()
        val password = et_password.text.toString()

        presenter.performLogin(email, password)
    }
}