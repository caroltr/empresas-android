package com.ioasys.empresas.login

import com.ioasys.empresas.model.Credential
import com.ioasys.empresas.model.HeaderAuth
import com.ioasys.empresas.network.ApiFactory
import com.ioasys.empresas.repository.Preferences
import com.ioasys.empresas.util.AppConstants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class LoginPresenter(private val view: LoginContract.View) : LoginContract.Presenter {

    override fun performLogin(email: String, password: String) {
        view.startProgress()

        val credentials = Credential(email, password)
        ApiFactory.api.signIn(credentials)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                this::onSuccess,
                this::onError
            )
    }

    private fun onSuccess(response: Response<Any>) {
        view.endProgress()

        if (response.isSuccessful) {
            getHeaders(response)?.let {
                Preferences.saveAuthHeaders(view.getContext(), it)
            }

            view.navigateMainView()

        } else {
            // TODO create error and avoid using null
            onError(null)
        }
    }

    private fun onError(error: Throwable?) {
        view.endProgress()
        view.displayError()
    }

    private fun getHeaders(response: Response<Any>): HeaderAuth? {
        val header = response.headers()

        val accessToken = header[AppConstants.accessTokenKey]
        val client = header[AppConstants.clientKey]
        val uid = header[AppConstants.uidKey]

        if (accessToken.isNullOrEmpty()
            || client.isNullOrEmpty()
            || uid.isNullOrEmpty()) {

            // TODO improve this, and avoid returning null
            return null
        }

        return HeaderAuth(accessToken, client, uid)
    }
}