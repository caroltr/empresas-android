package com.ioasys.empresas.network

import com.ioasys.empresas.model.Credential
import com.ioasys.empresas.model.Enterprises
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface Api {

    @POST("users/auth/sign_in")
    fun signIn(@Body login: Credential): Single<Response<Any>>

    @GET("enterprises")
    fun enterpriseFilter(
        @HeaderMap headers: Map<String, String>,
        @Query("name") name: String): Single<Response<Enterprises>>
}