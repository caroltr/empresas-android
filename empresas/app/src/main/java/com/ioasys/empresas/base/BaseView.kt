package com.ioasys.empresas.base

interface BaseView<T : BasePresenter> {
    fun setPresenter(presenter: T)
}