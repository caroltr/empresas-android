package com.ioasys.empresas.repository

import android.content.Context
import com.google.gson.Gson
import com.ioasys.empresas.model.HeaderAuth

object Preferences {

    private val gson = Gson()

    private var PRIVATE_MODE = 0
    private val PREF_NAME = "com.ioasys.empresas.prefs"
    private val AUTH = "auth"

    fun saveAuthHeaders(context: Context, auth: HeaderAuth) {
        val authStr = gson.toJson(auth)

        val sharedPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        val editor = sharedPref.edit()
        editor.putString(AUTH, authStr)
        editor.apply()
    }

    fun getAuthHeaders(context: Context): HeaderAuth {
        val sharedPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        val authStr = sharedPref.getString(AUTH, "")

        return gson.fromJson(authStr, HeaderAuth::class.java)
    }
}