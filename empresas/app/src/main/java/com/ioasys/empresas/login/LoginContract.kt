package com.ioasys.empresas.login

import android.content.Context
import com.ioasys.empresas.base.BasePresenter
import com.ioasys.empresas.base.BaseView

interface LoginContract {

    interface Presenter : BasePresenter {
        fun performLogin(email: String, password: String)
    }

    interface View : BaseView<Presenter> {
        fun navigateMainView()
        fun getContext(): Context
        fun startProgress()
        fun endProgress()
        fun displayError()
    }
}