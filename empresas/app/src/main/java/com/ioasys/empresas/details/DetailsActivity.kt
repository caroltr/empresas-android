package com.ioasys.empresas.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.ioasys.empresas.R
import com.ioasys.empresas.model.Enterprise
import com.ioasys.empresas.util.AppConstants
import com.ioasys.empresas.util.loadUrl
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.item_toolbar.*

class DetailsActivity : AppCompatActivity(), DetailsContract.View {

    private lateinit var presenter: DetailsContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        addToolbar()
        setPresenter(DetailsPresenter(this))

        presenter.start(intent.extras)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun setPresenter(presenter: DetailsContract.Presenter) {
        this.presenter = presenter
    }

    override fun displayDetails(enterprise: Enterprise) {
        supportActionBar?.apply {
            title = enterprise.enterprise_name
            setDisplayShowTitleEnabled(true)
        }

        tv_description.text = enterprise.description

        enterprise.photo?.let {
            val url = "${AppConstants.baseUrl}$it"
            Glide.with(this)
                .loadUrl(url)
                .into(iv_photo)
        }
    }

    private fun addToolbar() {
        toolbar?.let {
            setSupportActionBar(it)
            iv_logo.visibility = View.GONE
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
