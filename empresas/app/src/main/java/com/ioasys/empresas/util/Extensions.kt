package com.ioasys.empresas.util

import android.graphics.drawable.Drawable
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.ioasys.empresas.R

fun RequestManager.loadUrl(url: String): RequestBuilder<Drawable> {
    return this.load(url)
        .apply(RequestOptions()
        .placeholder(R.drawable.ic_image_placeholder))
}