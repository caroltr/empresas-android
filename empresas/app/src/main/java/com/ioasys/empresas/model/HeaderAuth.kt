package com.ioasys.empresas.model

data class HeaderAuth (
    val accessToken: String,
    val client: String,
    val uid: String
)