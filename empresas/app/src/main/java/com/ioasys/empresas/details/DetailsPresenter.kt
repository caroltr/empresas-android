package com.ioasys.empresas.details

import android.os.Bundle
import com.ioasys.empresas.model.Enterprise

class DetailsPresenter(private val view: DetailsContract.View): DetailsContract.Presenter {

    override fun start(bundle: Bundle?) {
        bundle?.let {
            val enterprise: Enterprise = it.getParcelable<Enterprise>("enterprise_key") as Enterprise
            view.displayDetails(enterprise)
        } ?: run {
            // TODO handle error
        }
    }
}