package com.ioasys.empresas.util

object AppConstants {
    const val baseUrl = "https://empresas.ioasys.com.br"
    const val baseUrlFull = "https://empresas.ioasys.com.br/api/v1/"

    const val accessTokenKey = "access-token"
    const val clientKey = "client"
    const val uidKey = "uid"

    const val enterpriseKey = "enterprise_key"

}