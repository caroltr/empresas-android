package com.ioasys.empresas.network

import com.ioasys.empresas.util.AppConstants
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object ApiFactory {

    private fun retrofit() : Retrofit = Retrofit.Builder()
        .baseUrl(AppConstants.baseUrlFull)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    val api : Api = retrofit().create(
        Api::class.java)
}