package com.ioasys.empresas.model

import com.google.gson.annotations.SerializedName

data class Credential(
    @SerializedName("email") val email: String,
    @SerializedName("password") val password: String
)